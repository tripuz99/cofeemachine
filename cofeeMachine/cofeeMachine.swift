//
//  cofeeMachine.swift
//  cofeeMachine
//
//  Created by dev12 on 9/3/18.
//  Copyright © 2018 dev12. All rights reserved.
//

import UIKit
var water = 100
var beans = 10
class cofeeMachine: NSObject {
    func addWater() -> String {
        water += 100
        return ("Water added")
    }
    func addBeans() -> String {
        beans += 10
        return ("Beans added")
    }
    func makeLatte() -> String {
        if beans < 20 {
            return ("Missing Beans")
        }
        if water < 200 {
            return ("Missing Water")
        }
        else {
            beans -= 20
            water -= 200
            return ("Drink your Latte, Bro")
        }
    }
    func makeEspresso() -> String {
        if beans < 30 {
            return ("Missing Beans")
        }
        if water < 150 {
            return ("Missing Water")
        }
        else {
            beans -= 30
            water -= 150
            return ("Drink your Espresso, Bro")
        }
    }
}
