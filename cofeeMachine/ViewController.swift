//
//  ViewController.swift
//  cofeeMachine
//
//  Created by dev12 on 9/3/18.
//  Copyright © 2018 dev12. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let makeCofee = cofeeMachine.init()
    @IBOutlet weak var waterCounter: UILabel!
    @IBOutlet weak var cofeeStatusLabel: UILabel!
    @IBOutlet weak var beansCounter: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad() 
        cofeeStatusLabel.text = "Hello, Bro"
        waterCounter.text = "Water = \(water)"
        beansCounter.text = "Beans = \(beans)"
    }
    @IBAction func makeLatteButton() {
        cofeeStatusLabel.text = makeCofee.makeLatte()
        waterCounter.text = "Water = \(water)"
        beansCounter.text = "Beans = \(beans)"
    }
    @IBAction func makeEspressoButton() {
        cofeeStatusLabel.text = makeCofee.makeEspresso()
        waterCounter.text = "Water = \(water)"
        beansCounter.text = "Beans = \(beans)"
    }
    @IBAction func addBeansButton() {
        cofeeStatusLabel.text = makeCofee.addBeans()
        beansCounter.text = "Beans = \(beans)"
    }
    @IBAction func addWaterButton() {
        cofeeStatusLabel.text = makeCofee.addWater()
        waterCounter.text = "Water = \(water)"
        beansCounter.text = "Beans = \(beans)"
    }

}

